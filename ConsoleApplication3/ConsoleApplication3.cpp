﻿
#include <iostream>

int main()
{
    std::string first = "452352";
    std::cout << "all " << first << "\n";
    std::cout << "length " << first.length() << "\n";
    std::cout << "first " << first[0] << "\n";
    std::cout << "last " << first[first.length() - 1] << "\n";

}
